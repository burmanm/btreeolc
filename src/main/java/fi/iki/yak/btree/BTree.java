/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fi.iki.yak.btree;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/**
 * @author michael
 */
public class BTree<K extends Comparable<K>, V> {

    volatile NodeBase root;

    public BTree() {
        root = new BTreeLeaf<K, V>();
    }

    void makeRoot(K key, NodeBase leftChild, NodeBase rightChild) {
        NodeBase currentRoot = this.root;
        BTreeInner<K> inner = new BTreeInner<>();
        inner.count = 1;
        inner.keys.set(0, key);
        inner.children.set(0, leftChild);
        inner.children.set(1, rightChild);
        rootUpdater.compareAndSet(this, currentRoot, inner);
    }

    void yield(int count) {
        if (count > 3)
            Thread.yield();
//        else
        // mm_pause(); TODO (Unsafe.park?)
    }

    private AtomicReferenceFieldUpdater<BTree, NodeBase> rootUpdater = AtomicReferenceFieldUpdater.newUpdater(BTree
            .class, NodeBase.class, "root");
}

/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fi.iki.yak.btree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author michael
 */
public class BTreeInner<K extends Comparable<K>> extends NodeBase {
    static PageType TYPE_MARKER = PageType.BTreeInner;

    ArrayList<K> keys;
    ArrayList<NodeBase> children;

    BTreeInner()
    {
        MAX_ENTRIES = 64; // TODO Define this..
        count = 0;
        type = TYPE_MARKER;
        keys = new ArrayList<>(MAX_ENTRIES);
        children = new ArrayList<>(MAX_ENTRIES); // this can grow
    }

    private boolean isFull() {
        return count == MAX_ENTRIES - 1;
    }

    private int lowerBound(K key) {
        int lower = 0;
        int upper = count;

        do {
            int mid = ((upper - lower) / 2) + lower;
            K midKey = keys.get(mid);

            int comparison = key.compareTo(midKey);
            if(comparison < 0) {
                upper = mid;
            } else if(comparison > 0) {
                lower = mid + 1;
            } else {
                return mid;
            }
        } while(lower < upper);
        return lower;
    }

    void insert(K key, NodeBase child) {
        // void * memmove ( void * destination, const void * source, size_t num );
/*
    assert(count < maxEntries - 1);
    unsigned pos = lowerBound(k);
    memmove(keys + pos + 1, keys + pos, sizeof(Key) * (count - pos + 1));
    memmove(children + pos + 1, children + pos, sizeof(NodeBase *) * (count - pos + 1));
    keys[pos] = k;
    children[pos] = child;
    std::swap(children[pos], children[pos + 1]);
    count++;
 */
        int pos = lowerBound(key);
        for(int i = count - 1; i > pos; i--) {
            keys.set(i+1, keys.get(i));
            children.set(i+1, children.get(i));
        }
        keys.set(pos, key);
        children.set(pos, child);
        Collections.swap(children, pos, pos + 1); // Why the swap afterwards? I should rather do it right away ?
        count++; // Should this be atomic?
    }

    /*
    TODO AtomicReference is probably not needed
     */
    BTreeInner<K> split(AtomicReference<K> sepKey) {
        BTreeInner<K> newInner = new BTreeInner<>();
        newInner.setCount(count - (count / 2));
        count = count - newInner.getCount() - 1;
        sepKey.set(keys.get(count));


        /*
        for(int i = 0; i < newLeaf.getCount(); i++) {
            newLeaf.data.set(i, data.get(count+i));
        }

        memcpy(newInner->keys, keys + count + 1, sizeof(Key) * (newInner->count + 1));
        memcpy(newInner->children, children + count + 1, sizeof(NodeBase *) * (newInner->count + 1));
        return newInner;
        */

        // TODO In off-heap case, use copyMemory, or Arrays.copyRange if no ArrayList..
        for(int i = 0; i < newInner.getCount() + 1; i++) {
            newInner.keys.set(i, keys.get(count + i + 1));
            newInner.children.set(i, children.get(count + i + 1));
        }

        return newInner;
    }
}

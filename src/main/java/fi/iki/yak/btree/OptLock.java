package fi.iki.yak.btree;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Direct port of BTreeOLC from https://github.com/wangziqi2016/index-microbench/blob/master/BTreeOLC/
 *
 * @author michael
 */
public class OptLock {

    // TODO Check StampedLock (benefits automatically from Java 9 upgrade also)

    private static long IS_LOCKED = 0x04;
    private AtomicLong typeVersionLockObsolete = new AtomicLong(IS_LOCKED);

    boolean isLocked(long version) {
        return (version & IS_LOCKED) == IS_LOCKED;
    }

    /*
     TODO Remove AtomicBoolean overhead
     This is just copy of the original, it updates a boolean pointer (..why..)
     Needs to return both boolean as well as version
      */
    long readLockOrRestart(AtomicBoolean needRestart) {
        long version = typeVersionLockObsolete.get();
        if(isLocked(version) || isObsolete(version)) {
            // mm_pause(); (Unsafe.park?) https://bugs.openjdk.java.net/browse/JDK-8147832 Thread.onSpin.. JDK 9,
            // Unsafe.unpark also (see StampedLock .. or maybe use that one instead of this)
            needRestart.set(true);
        }
        return version;
    }

    /*
    TODO Remove AtomicBoolean overhead, this can just return a boolean and process booleans
     */
    void writeLockOrRestart(AtomicBoolean needRestart) {
        long version = readLockOrRestart(needRestart);
        if(needRestart.get()) {
            return;
        }

        upgradeToWriteLockOrRestart(version, needRestart);
        if(needRestart.get()) {
            return;
        }
    }

    /*
    TODO Remove AtomicBoolean and AtomicLong and make them more sane..
     */
    void upgradeToWriteLockOrRestart(AtomicLong version, AtomicBoolean needRestart) {
        long currentVersion = version.get();
        if(typeVersionLockObsolete.compareAndSet(currentVersion, currentVersion + IS_LOCKED)) {
            version.set(currentVersion + IS_LOCKED);
        } else {
            // mm_pause(); TODO (Unsafe.park?)
            needRestart.set(true);
        }
    }

    void writeUnlock() {
        typeVersionLockObsolete.addAndGet(0x02);
    }

    boolean isObsolete(long version) {
        return (version & 1) == 1;
    }

    /*
     * TODO Remove AtomicBoolean
     */
    void checkOrRestart(long startRead, AtomicBoolean needRestart) {
        readUnlockOrRestart(startRead, needRestart);
    }

    /*
     * TODO Remove AtomicBoolean
     */
    void readUnlockOrRestart(long startRead, AtomicBoolean needRestart) {
        needRestart.set(startRead != typeVersionLockObsolete.get());
    }

    void writeUnlockObsolete() {
        typeVersionLockObsolete.addAndGet(0x03);
    }
}

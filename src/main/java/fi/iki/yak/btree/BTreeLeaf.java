/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fi.iki.yak.btree;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author michael
 */
public class BTreeLeaf<K extends Comparable<K>, V> extends NodeBase {
    static PageType TYPE_MARKER = PageType.BTreeLeaf;

    class Entry<K, V> {
        private K key;
        private V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }

    /*
    TODO Define MAX_ENTRIES

    This is age-old problem with Java.. we want 4kB/8kB pages for cache friendliness, but how do we ensure in Java
    that this happens? Especially with string keys..

    Should I do this in off-heap using ByteBuffers? At least in Cassandra they could be..

    Or use Unsafe? We probably want offheap & onheap versions..
     */



    /**
     * TODO Create more efficient storage for data. This is to simplify implementation
     * Check https://github.com/justinsb/cloudata/tree/master/cloudata-btree/src/main/java/com/cloudata/btree
     * for comparison
     * And internal BTree also
     */
    ArrayList<Entry<K, V>> data;

    public BTreeLeaf() {
        MAX_ENTRIES = 64; // TODO Define this..
        count = 0;
        type = TYPE_MARKER;
        data = new ArrayList<>(MAX_ENTRIES);
    }

    private boolean isFull() {
        return count == MAX_ENTRIES;
    }

    private int lowerBound(K key) {
        int lower = 0;
        int upper = count;

        do {
            int mid = ((upper - lower) / 2) + lower;
            K midKey = data.get(mid).getKey();

            int comparison = key.compareTo(midKey);
            if(comparison < 0) {
                upper = mid;
            } else if(comparison > 0) {
                lower = mid + 1;
            } else {
                return mid;
            }
        } while(lower < upper);
        return lower;
    }

    public void insert(K key, V value) {
        Entry<K, V> e = new Entry<>(key, value);
        if(count > 0) {
            int pos = lowerBound(key);
            if(pos < count && (key == data.get(pos).key)) {
                // Update the value only (upsert)
                data.set(pos, e);
            }
            // Insert before current pos and move everything one right
            for(int i = count - 1; i > pos; i--) {
                data.set(i+1, data.get(i));
            }
            data.set(pos, e);
        } else {
            // First item
            data.set(0, e);
        }
        count++;
    }

    /*
    TODO Is AtomicReference really needed?
     */
    public BTreeLeaf<K, V> split(AtomicReference<K> sepKey) {
        BTreeLeaf<K, V> newLeaf = new BTreeLeaf<>();
        newLeaf.setCount(count - (count / 2));
        count = count - newLeaf.getCount();
        for(int i = 0; i < newLeaf.getCount(); i++) {
            newLeaf.data.set(i, data.get(count+i));
        }
        // Why is separator needed?
        sepKey.set(data.get(count - 1).getKey());
        return newLeaf;
    }
}
